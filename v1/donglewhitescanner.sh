#!/bin/bash
#-x for debug -e to exit on error -v to print everything
#Script that execs the rtl dongle scanner

rmmod

TIMESTAMP=$(date -I)
ROUTE=/home/pi/${TIMESTAMP}

function check_devices(){
	rtl=$(lsusb | grep RTL)
	gps=$(lsusb | grep PL2303)

	if [ "$rtl" ] ; then
		if [ "$gps" ]; then
			echo 0 
		else
			echo 1
		fi 
	else
		echo 2
	fi
}

function run_scanner(){
	local unique_file=$(date +"%T")
	rtlsdr_scan.py -s ${1} -e ${2} -w ${3} -d 0.008 -f 4 -c /opt/rtl/RTLSDR-Scanner/src/gps.conf ${4}/${unique_file}.rfs
}

function create_directory(){
	if [ -d /home/pi/${TIMESTAMP} ] ; then
		continue
	else
		#there is no directory for this date, it creates a directory for all measures of the same date 
		if [ $(mkdir $ROUTE) ] ; then
			echo "Measure directory created"
		else
			echo "Error creating directory"
			exit 1
		fi
	fi	
}

###################################################

var=$(check_devices)

case "$var" in
	0 )
		create_directory
		
			while true 
				do 
					echo "Starting measures"
					run_scanner 92 93 1 $ROUTE
					if [ $? -eq "0" ]; then
						echo "Success"
					else
						echo "Error executing function"
						# exit 2
						break
					fi
				done
		;;
	1)
		echo "GPS not connected"
		;;
	2)
		echo "RTL not connected"
		;; 
	*)
		logger -i -t donglewhitescanner "unexpected error"
		echo "unexpected error"
		;;	
esac



exit 0